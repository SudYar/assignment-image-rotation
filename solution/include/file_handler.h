#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H

#include "image.h"

enum read_status{
    READ_OK = 0,
    READ_ERROR,
    READ_INVALID_FILE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status{
    WRITE_OK = 0,
    WRITE_INVALID_FILE,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BITS,
    WHITE_ERROR
};

enum read_status in_bmp (FILE *in, struct image *img);

enum write_status out_bmp (FILE *out, struct image *img);

#endif //FILE_HANDLER_H
