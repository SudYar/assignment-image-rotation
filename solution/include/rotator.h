#ifndef LAB3_ROTATOR_H
#define LAB3_ROTATOR_H
#include "image.h"
#include "malloc.h"

struct image rotate( struct image img);

#endif //LAB3_ROTATOR_H
