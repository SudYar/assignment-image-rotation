#include <stdio.h>

#include "file_handler.h"
#include "image.h"
#include "malloc.h"




uint32_t padding = 0;
static void padding_set(uint32_t width) {
    padding = width % 4;
}

static enum read_status img_in_data(FILE *in, struct image *img) {
    padding_set(img->width);

    struct pixel *px_data = malloc(sizeof (struct pixel) * (padding + img->width) * img->height);
    struct pixel *px_2 = px_data;

    for (uint64_t i = 0; i < img->height; i++){
        if (fread(px_2, sizeof (struct pixel), img->width, in) != img->width ||
                fseek(in, padding, 1) != 0){
            free(px_data);
            return READ_INVALID_BITS;
        }
        px_2 += img->width;
    }
    img->data = px_data;
    return READ_OK;
}

static enum write_status img_out_data(FILE *out, struct image *img){
    padding_set(img->width);

    struct pixel *px_2 = img->data;

    for (uint64_t i = 0; i < img->height; i++){
        if (fwrite(px_2, sizeof (struct pixel), img->width, out) != img->width ||
            fseek(out, padding, 1) != 0){
            return WRITE_INVALID_BITS;
        }
        px_2 += img->width;
    }
    return WRITE_OK;
}


static struct bmp_header new_bmp_header(const struct image *img) {
    return (struct bmp_header) {
            .bfType = HEADER_TYPE,
            .bfileSize = STRUCT_BMP_HEADER_SIZE + img->width*img->height*sizeof(struct pixel) + padding*img->height,
            .bfReserved = 0,
            .bOffBits = STRUCT_BMP_HEADER_SIZE,
            .biSize = BIT_HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = img->width * img->height * sizeof(struct pixel) + padding * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static enum read_status check_header(struct bmp_header header) {
    if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    }
    if (header.bfType != HEADER_TYPE){
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}


enum read_status in_bmp (FILE *in, struct image *img){
    if (in == NULL) {
        return READ_INVALID_FILE;
    }

    struct bmp_header header = {0};
    if (fread(&header, STRUCT_BMP_HEADER_SIZE, 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    enum read_status status_header = check_header(header);
    if (status_header != 0){
        return status_header;
    }


    img->height = header.biHeight;
    img->width = header.biWidth;
    padding_set(img->width);

    return img_in_data(in, img);

}

enum write_status out_bmp(FILE *out, struct image *img ) {
    if (out == NULL){
        return WRITE_INVALID_FILE;
    }

    struct bmp_header bmpHeader = new_bmp_header(img);
    if (fwrite(&bmpHeader, bmpHeader.bOffBits, 1, out) != 1) {
        return WRITE_INVALID_HEADER;
    }

    return img_out_data(out, img);


}



