#include "rotator.h"

struct image rotate(struct image img){
    struct image img_double = {.width = img.height, .height = img.width,
            .data = malloc(img.width*img.height*sizeof (struct pixel))};

    for (uint64_t i = 0; i < img.height; i++){
        for ( uint64_t j = 0; j < img.width; j++) {
            img_double.data[(img.height - i - 1) + j * img.height] = img.data[i*img.width + j];
        }
    }

    return img_double;
}

