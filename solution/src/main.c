#include <stdio.h>

#include "print_msg.h"
#include "file_handler.h"
#include "image.h"
#include "rotator.h"

FILE* open_in_FILE(char *path){
    return fopen(path, "rb");
}

FILE* open_out_FILE(char *path) {
    return fopen(path, "wb");
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3){
        err_println("Need only 2 arguments");
        return 1;
    }

    FILE *in = open_in_FILE(argv[1]);
    struct image img = {0};
    enum read_status rd_status = in_bmp(in, &img);
    fclose(in);
    if (rd_status != 0) {
        err_println("Read failed");
        return 1;
    }

    struct image rotate_img = rotate(img);
    FILE *out = open_out_FILE(argv[2]);
    enum write_status wt_status = out_bmp(out, &rotate_img);
    fclose(out);
    if (wt_status != 0) {
        err_println("Write failed");
        return 1;
    }


    free(img.data);
    free(rotate_img.data);
    out_println("success");
    return 0;

    
}

